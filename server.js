const bodyParser = require('body-parser')
const express = require('express')
const port = 3000
const { MongoClient, ObjectID } = require('mongodb');

//const uri = "mongodb+srv://user:calendarapp@cluster0.lh7p9.mongodb.net/calendar?retryWrites=true&w=majority";

const uri = "mongodb://localhost:27017/calendar"; 


const client = new MongoClient(uri, { useUnifiedTopology: true}, { useNewUrlParser: true }, { connectTimeoutMS: 30000 }, { keepAlive: 1});


const app = express()
var jsonParse = bodyParser.json();

client.connect(err => {
  const db = client.db("calendar")
  const collection = db.collection("events");

  // perform actions on the collection object
  app.get('/api/events', (req, res, next) => {
    console.log('GET');
    collection.find({}).toArray((err, results) => {
      if(err){
        next(err);
      }
      res.send(results);
    });
  })

  app.post('/api/events', jsonParse, (req, res, next) => {
    console.log('POST');
    console.log(req.body);
    collection.insertOne(req.body).then(
      results => res.status(201).send(results.insertedId),
      err => next(err),
    );
  })

  app.put('/api/events', jsonParse, (req, res, next) => {
    let id = req.query._id;
    id = new ObjectID(id);
    console.log('PUT -- id: ' + id);
    console.log(req.body);
    collection.updateOne(
      {
        _id: id
      },
      {
        $set: req.body
      }).then(
        (results) => {
          if(results.result.nModified > 0){
            res.status(200).send();
          }else{
            res.status(204).send();
          }
        },
        (err) => next(err),
      );
  })

  app.delete('/api/events', (req, res, next) => {
    let id = req.query._id;
    id = new ObjectID(id);
    let query = {_id: id};
    console.log('DELETE');
    console.log(query);
    collection.deleteOne({_id: id}).then(
      (result) =>  {
        if(result.deletedCount > 0){
          res.status(200).send();
        }else{
          res.status(204).send();
        }
      },
      (err) => next(err),
    );
  })
});

app.use(express.static('./dist/Calendar'));
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
