import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  get_events(): Observable<any>{
    return this.http.get('/api/events');
  }

  set_event(event){
    let options = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return this.http.post('/api/events', event, options);
  }

  put_event(changed_event, id){
    let options = {
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        _id: id,
      }
    }
    console.log(options);
    return this.http.put('/api/events', changed_event, options);
  }

  delete_event(id){
    let options = {
      params: {
        _id: id,
      }
    }
    console.log(options);
    return this.http.delete('/api/events', options);
  }
}
