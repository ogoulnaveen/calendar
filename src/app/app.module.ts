import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FullCalendarModule } from '@fullcalendar/angular'; // for FullCalendar!
import { AppComponent } from './app.component';
import { EventPopupComponent } from './event-popup/event-popup.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DateValidatorDirective } from './date-validator.directive';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    EventPopupComponent,
    DateValidatorDirective,
  ],
  imports: [
    BrowserModule,
    FullCalendarModule, // for FullCalendar!
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
